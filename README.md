**Penceo Profile**

**Type:** Drupal Profile
**Version:** 2.51.3-7.61

**Changelog 2.51.3-7.61:**
 - Add extra rules for video content in Wysiwyg filter

**Changelog 2.51.2-7.61:**
 - Drupal 7.61 support
 - Added header-button-* class wildcard for wysiwyg filter

**Changelog 2.51-7.58:**

 - Add drd_agent => 3.5 for monitoring.

**Changelog 2.50.1-7.58:**
 
 - Change iso3 code for Kosovo.

**Changelog 2.50-7.58:**

 - Change window resize to resizeend for the cs adaptive image formatter.
 - Lazyload is now part of the profile also has a formatting option for cs adaptive.
 - Fix notice for cs adaptive image when no alt or title attribute was given.
 - Penceo IPE templates v2 is now functional
 - FPP 1.10 => 1.1
 - Fix "An illegal choice has been detected" views error.
 - wysiwyg_filter patch is removed
 - Make sure that the flag module checks for existing flag png first also add fallback and "unknown" option.
 - Kosovo is added properly to the country list.
 - Several contrib security updates
    - Security update for: title "1.0-alpha8" => "1.0-alpha9"
    - Security update for: entityreference "1.1" => "1.5"
    - Security update for: autologout "4.4" => "4.5"
    - Security update for: backup_migrate: version: "3.1" => "~" - Not critical module must not be specified.
    - Security update for: views: version: "3.14" => "3.18"
    - Security update for: better_exposed_filters: "3.2" => "3.5"
    - Security update for: search_api_sorts "1.6" => "1.7"
    - Security update for: elysia_cron "2.3" => "2.4"
 
**Changelog 2.42-7.58:**
 - Tested with https://www.drupal.org/sa-core-2018-002 security update
 - Merged properly with 2.41-7.54

**Changelog 2.41-7.54:**
 - Added patch to redirect module

**Changelog 2.4-7.54:**

 - penceo_get_taxonomy_term_options fixes.
 - penceo_field_instance_delete new cleanup parameter that purges field cache
 - p tag is added to penceo_tags
 - new function: penceo_array_search_recursive - Allows recursive search in N depth arra
 - penceo-cs-adaptive-image.js is now added to the profile - supports:
    - lazyload
    - proper responsive behavior
    - focus point fixes
 - penceo_cs_adaptive_image_formatter:
    - now supports title wrapping
    - now supports DailyMotion stream wrapper
    - now supports title addition
    - now supports customized classes
    - new function: penceo_core_field_formatter_cs_adaptive_style_normalizer
    - fragment and query string support added
    - theme changes
 - new element validate: penceo_element_validate_link
 - new element validate: 
 - new ctools access plugin: Field set validate.
 - penceo_wysiwyg filters finetuned
 - linkit support properly added
 - pathologic is now added to make
 - administerusersbyrole is now added to make
 - role_delegation is now added to make
 - redirect is now added to make
 - linkchecker is now added to make

**Changelog 2.3-7.50:**

 - Batch processor added to profile.
 - Penceo flag now suppors several tags.
 - Geolocation module is patched so it works properly now with a valid gapi code.
 - Media browser plus helper taxonomy helper added to helpers.
