<?php

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function penceo_profile_form_install_configure_form_alter(&$form, $form_state) {
  // Set default site name.
  $form['site_information']['site_name']['#value'] = 'Penceo Profile Install';
  // Set default site email.
  $form['site_information']['site_mail']['#value'] = 'info@penceo.com';
}