<?php
/**
 * @file
 * theme file for penceo_flags.
 */

/**
 * Returns a country html element that is capable of flag display
 * of image sprites.
 *
 * The following variables exist:
 * icon_size: int      The size of the flag icon.
 * icon_iso2: string   The iso2 representation of the flag.
 * icon_suffix: string Optional suffix value of the flag.
 * html_tag: string    The value that wraps the
'html_tag' => '',
 *
 */
function theme_penceo_flag_icon($variables) {
  $flag_code = penceo_flags_image_code($variables['icon_iso2']);
  $elem = array(
    '#theme' => 'html_tag',
    '#tag' => $variables['html_tag'],
    '#value' => '',
    '#attributes' => array(
      'class' => array('flag-icon flag-iso2-h' . $variables['icon_size'] . '-' . $flag_code),
    ),
  );

  $suffix = '';
  if (!empty($variables['icon_suffix'])) {
    $country = country_load($variables['icon_iso2']);
    switch ($variables['icon_suffix']) {
      case 'iso2':
        $suffix = $country->iso2;
        break;

      case 'iso3':
        $suffix = $country->iso2;
        break;

      case 'ioc':
        ctools_include('helpers', 'penceo_core');
        $suffix = penceo_ioc_by_iso($country->iso3);
        break;

      case 'name':
        $suffix = $country->name;
        break;
    }

    $elem = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array('flag-icon-wrapper'),
      ),
      'icon' => $elem,
      'suffix' => array(
        '#theme' => 'html_tag',
        '#tag' => $variables['html_tag'],
        '#value' => check_plain($suffix),
        '#attributes' => array(
          'class' => array('flag-icon-suffix'),
        ),
      ),
    );
  }

  return render($elem);
}
