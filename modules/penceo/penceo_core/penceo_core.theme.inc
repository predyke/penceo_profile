<?php
/**
 * @file
 * Custom formatters theme file.
 */

/**
 * Returns HTML for an adaptive image formatter.
 *
 * NOTE: This is a custom implementation of
 * the cs_adaptive_image_formatter theme.
 * The implementation includes the following differences.
 *  - Additional classes can be added when a link wrapper is specified.
 *  - Focuspoint and focalpoint module implementations.
 *
 * @param array $variables
 *   An associative array containing:
 *   - item: An array of image data.
 *   - path: An array containing the link 'path' and link 'options'.
 *   - styles: An array of styles, keyed by break point width.
 *   - max_style: The image style to use for any larger width.
 *   - fallback_style: The fallback image style.
 *
 * @see cs_adaptive_image.module
 *
 * @ingroup themeable
 *
 * @return string
 *   The generated image tag.
 */
function theme_penceo_cs_adaptive_image_formatter($variables) {
  $item = $variables['item'];
  $image = array(
    'path' => $item['uri'],
  );
  if (isset($item['alt'])) {
    $image['alt'] = $item['alt'];
  }

  // Title attribute, if not empty.
  if (isset($item['title']) && drupal_strlen($item['title']) > 0) {
    $image['title'] = $item['title'];
  }

  // Width and height attributes.
  if (isset($item['width']) && isset($item['height'])) {
    $image['width'] = $item['width'];
    $image['height'] = $item['height'];
  }

  $image['lazyload'] = $variables['lazyload'];

  // Attach focus points coordinates to img.
  if (module_exists('focal_point')) {

    ctools_include('helpers', 'penceo_core');

    $focal_point = focal_point_get($item['fid']);
    // Focal point does not always exist.
    $focal_point = $focal_point ? explode(',', $focal_point) : array(50, 50);
    list($left, $top) = $focal_point;
    list($x, $y) = _penceo_generate_focus_point($left, $top);
    $attributes['data-focus-x'] = $x;
    $attributes['data-focus-y'] = $y;
  }

  // Output fallback image that will work without JavaScript.
  $image_output = _penceo_core_adaptive_image_view($image, $variables['fallback_style']);

  // Prepare attributes that will be picked up by our JavaScript code
  // to serve an adapted image. Some browsers cannot access the
  // children of a <noscript> element, thus all the data needs to be
  // attached to the <noscript> element.
  $image['attributes']['class'][] = 'adaptive-image';
  $attributes['class'][] = 'adaptive-image';
  if (isset($variables['breakpoint_styles'])) {
    foreach ($variables['breakpoint_styles'] as $breakpoint => $style_name) {
      $image['attributes']['data-adaptive-image-breakpoint'] = $breakpoint;
      // Append to list of breakpoints.
      $attributes['data-adaptive-image-breakpoints'][] = $breakpoint;
      $attributes['data-adaptive-image-' . $breakpoint . '-img'] = _penceo_core_adaptive_image_view($image, $style_name);
    }
  }
  $image['attributes']['data-adaptive-image-breakpoint'] = 'max';
  $attributes['data-adaptive-image-max-img'] = _penceo_core_adaptive_image_view($image, $variables['max_style']);

  // Output the <noscript> element with its data attributes.
  $output = theme(
    'html_tag', array(
      'element' => array(
        '#tag' => 'noscript',
        '#value' => $image_output,
        '#attributes' => $attributes,
      ),
    )
  );

  // Title add to output.
  if ($variables['title_wrapper']) {
    $output .= theme(
      'html_tag', array(
        'element' => array(
          '#tag' => $variables['title_wrapper'],
          '#value' => $variables['title'],
          '#attributes' => array(
            'class' => array('element-title'),
          ),
        ),
      )
    );
  }
  // Output as link, if required.
  if (isset($variables['path']['path'])) {
    $link_variables = array(
      'html' => TRUE,
    );

    if (!empty($variables['class'])) {
      $link_variables['attributes']['class'] = $variables['class'];
    }
    if (isset($variables['path']['options']['target'])) {
      $link_variables['attributes']['target'] = $variables['path']['options']['target'];
    }
    if (isset($variables['path']['options']['fragment'])) {
      $link_variables['fragment'] = $variables['path']['options']['fragment'];
    }
    if (isset($variables['path']['options']['query'])) {
      $link_variables['query'] = $variables['path']['options']['query'];
    }

    $output = l($output, $variables['path']['path'], $link_variables);
  }

  return $output;
}

/**
 * Changes focal point coordinates to focus point.
 *
 * @param int $left
 *    Coordinate from image left.
 * @param int $top
 *    Coordinate from image top.
 *
 * @return array coordinates
 *    An array with x, y coordinates for focus point.
 */
function _penceo_generate_focus_point($left = 50, $top = 50) {
  $x = ($left - 50) / 50;
  $y = (50 - $top) / 50;
  return array($x, $top < 0 ? abs($y) : $y);
}

/**
 * Handles image manipulation for theme_penceo_cs_adaptive_image_formatter.
 *
 * Based on cs_adaptive_image.
 *
 * @param array $image
 *    Stores information about the image.
 * @param string $style_name
 *    The name of the style.
 * @param bool $frontend_processing
 *    When enabled the img src will be output via a data-attr.
 *
 * @return string
 *    The generated html for the image.
 */
function _penceo_core_adaptive_image_view($image, $style_name, $frontend_processing = FALSE) {
  drupal_add_js(drupal_get_path('module', 'cs_adaptive_image') . '/cs_adaptive_image.js', 'file');

  if ($style_name) {
    $variables = $image;
    $variables['style_name'] = $style_name;
    // Determine the dimensions of the styled image.
    $dimensions = array(
      'width' => $variables['width'],
      'height' => $variables['height'],
    );

    image_style_transform_dimensions($variables['style_name'], $dimensions);

    $variables['width'] = $dimensions['width'];
    $variables['height'] = $dimensions['height'];

    // Determine the URL for the styled image.
    $variables['path'] = image_style_url($variables['style_name'], $variables['path']);
    $attributes = !empty($variables['attributes']) ? $variables['attributes'] : array();

    if ($variables['lazyload']) {
      $attributes['data-original'] = file_create_url($variables['path']);
      $attributes['src'] = "data:image/svg+xml;charset=utf-8,%3Csvg xmlns%3D'http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg' viewBox%3D'0 0 {$variables['width']} {$variables['height']}'%2F%3E";
    } else {
      $attributes['src'] = file_create_url($variables['path']);
    }

    foreach (array('width', 'height', 'alt', 'title') as $key) {
      if (isset($variables[$key])) {
        $attributes[$key] = $variables[$key];
      }
    }

    return '<img' . drupal_attributes($attributes) . ' />';
  }
  else {
    return theme('image', $image); // Original image.
  }
}