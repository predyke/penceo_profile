<?php
/**
 * @file
 * Penceo core field specific hooks and settings.
 */

/**
 * Implements hook_field_formatter_info().
 */
function penceo_core_field_formatter_info() {
  $formatters = array();

  if (module_exists('cs_adaptive_image')) {
    // Overriden adaptive formatter based on the original one.
    $formatters['penceo_cs_adaptive_image'] = array(
      'label' => t('PENCEO: Adaptive image'),
      'field types' => array('file', 'image'),
      'settings' => array(
        'image_link' => '',
        'styles' => '',
        'field_name' => '',
        'class' => '',
        'display_settings' => array(
          'offset' => '',
          'number_of_items' => '',
          'source_field' => 'none',
          'lazyload' => 0,
        ),
        'title_wrapper' => '',
      ),
    );
  }

  if (module_exists('countries')) {
    // Formatter that displays IOC code instead of ISO-3.
    $formatters['penceo_countries_ioc'] = array(
      'label' => t('PENCEO: IOC'),
      'field types' => array('country'),
    );
  }

  return $formatters;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function penceo_core_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $element = array();

  switch ($display['type']) {
    case 'penceo_cs_adaptive_image':

      $display = $instance['display'][$view_mode];
      $image_styles = image_style_options(FALSE);

      $link_types = penceo_core_field_formatter_get_link_types();

      $element = array();

      $element['image_link'] = array(
        '#title' => t('Link image to'),
        '#type' => 'select',
        '#default_value' => $settings['image_link'],
        '#description' => t('When selecting Link field all available link_field type fields will be available in the select list below.'),
        '#empty_option' => t('Nothing'),
        '#options' => $link_types,
      );

      $element['class'] = array(
        '#title' => t('Class'),
        '#description' => t('Add additional classes for the link that wraps the image. <i>Separated with spaces</i>'),
        '#type' => 'textfield',
        '#default_value' => $settings['class'],
        '#states' => array(
          'invisible' => array(
            ':input[name="image_link"]' => array('value' => ''),
          ),
        ),
      );

      $element['field_name'] = array(
        '#type' => 'select',
        '#title' => t('Field name'),
        '#description' => t('Add source field machine name for the displayed image.'),
        '#options' => penceo_core_field_formatter_get_source_fields('link_field'),
        '#default_value' => $settings['field_name'],
        '#states' => array(
          'visible' => array(
            ':input[name="image_link"]' => array('value' => 'field'),
          ),
        ),
      );

      // Settings for title display.
      $element['title_wrapper'] = array(
        '#title' => t('Title wrapper'),
        '#type' => 'select',
        '#default_value' => $settings['title_wrapper'],
        '#empty_option' => t('Hide title'),
        '#options' => array(
          'h1' => 'h1',
          'h2' => 'h2',
          'h3' => 'h3',
          'div' => 'div',
          'span' => 'span',
        ),
      );

      $element['display_settings'] = array(
        '#type' => 'fieldset',
        '#title' => t('Display settings'),
      );

      $element['display_settings']['offset'] = array(
        '#type' => 'textfield',
        '#title' => t('Offset'),
        '#description' => t('Define an offset where the display of images start.'),
        '#size' => 3,
        '#default_value' => $settings['display_settings']['offset'],
      );

      $element['display_settings']['number_of_items'] = array(
        '#type' => 'textfield',
        '#title' => t('Number of items'),
        '#description' => t('Display exactly the number of images set here. (When offset set above it will be calculated with that!)'),
        '#size' => 3,
        '#default_value' => $settings['display_settings']['number_of_items'],
      );

      $element['display_settings']['source_field'] = array(
        '#type' => 'select',
        '#title' => t('Source field'),
        '#description' => t('Add source field machine name for the displayed image.'),
        '#options' => array('none' => t('None')) + penceo_core_field_formatter_get_source_fields('image'),
        '#default_value' => $settings['display_settings']['source_field'],
      );

      $element['display_settings']['lazyload'] = array(
        '#type' => 'checkbox',
        '#title' => t('Lazyload'),
        '#description' => t('When checked the img src tags will be empty and the image paths will be given as data-attributes. This is useful not to load images on page request when not needed.'),
        '#default_value' => $settings['display_settings']['lazyload'],
      );

      $element['styles'] = array(
        '#tree' => TRUE,
        '#theme' => 'cs_adaptive_image_styles_form',
      );

      for ($i = 1; $i <= variable_get('cs_adaptive_image_breakpoint_count', 5); $i++) {
        $element['styles']['breakpoint_' . $i] = array(
          '#title' => t('Client width breakpoint @key', array('@key' => $i)),
          '#title_display' => 'invisible',
          '#type' => 'textfield',
          '#default_value' => isset($settings['styles']['breakpoint_' . $i]) ? $settings['styles']['breakpoint_' . $i] : '',
          '#maxlength' => 5,
          '#size' => 5,
          '#field_suffix' => 'px',
          '#element_validate' => array('element_validate_integer_positive'),
        );
        $element['styles']['style_' . $i] = array(
          '#title' => t('Image style for breakpoint @key', array('@key' => $i)),
          '#title_display' => 'invisible',
          '#type' => 'select',
          '#default_value' => isset($settings['styles']['style_' . $i]) ? $settings['styles']['style_' . $i] : '',
          '#empty_option' => t('None (original image)'),
          '#options' => $image_styles,
        );
      }

      $element['styles']['max_style'] = array(
        '#title' => t('Maximum'),
        '#title_display' => 'invisible',
        '#description' => t('Image style to use when the client width exceeds the widest value specified above.'),
        '#type' => 'select',
        '#default_value' => isset($settings['styles']['max_style']) ? $settings['styles']['max_style'] : '',
        '#empty_option' => t('None (original image)'),
        '#options' => $image_styles,
      );
      $element['styles']['fallback_style'] = array(
        '#title' => t('Fallback'),
        '#title_display' => 'invisible',
        '#description' => t('Image style to use when the client does not support JavaScript.'),
        '#type' => 'select',
        '#default_value' => isset($settings['styles']['fallback_style']) ? $settings['styles']['fallback_style'] : '',
        '#empty_option' => t('None (original image)'),
        '#options' => $image_styles,
      );

      break;
  }

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function penceo_core_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $instance['display'][$view_mode]['settings'];
  $summary = array();

  switch ($display['type']) {
    case 'penceo_cs_adaptive_image':

      for ($i = 1; $i <= variable_get('cs_adaptive_image_breakpoint_count', 5); $i++) {
        if (isset($settings['styles']['breakpoint_' . $i]) && isset($settings['styles']['style_' . $i])) {
          $breakpoint = $settings['styles']['breakpoint_' . $i];
          if ($breakpoint > 0) {
            $summary[] = t('Client widths up to @width px &rarr; Image style: @style', array(
              '@width' => $breakpoint,
              '@style' => _cs_adaptive_image_view_image_style($settings['styles']['style_' . $i]),
            ));
          }
        }
      }
      $summary[] = t('Maximum &rarr; Image style: @style', array('@style' => _cs_adaptive_image_view_image_style(isset($settings['styles']['max_style']) ? $settings['styles']['max_style'] : '')));
      $summary[] = t('Fallback &rarr; Image style: @style', array('@style' => _cs_adaptive_image_view_image_style(isset($settings['styles']['fallback_style']) ? $settings['styles']['fallback_style'] : '')));

      $link_types = penceo_core_field_formatter_get_link_types();

      // Display this setting only if image is linked.
      if (isset($link_types[$settings['image_link']]) && $settings['image_link'] == 'field') {
        $summary[] = t('Image link to <i>@field</i> machine names field.', array('@field' => $settings['field_name']));
      }
      else {
        $summary[] = t('Image link to @link.', array('@link' => $link_types[$settings['image_link']]));
      }
      if ($settings['class']) {
        $summary[] = t('Additional classes: @classes', array('@classes' => $settings['class']));
      }

      // Display title setting summary.
      if ($settings['title_wrapper']) {
        $summary[] = t('Display title with @tag wrapper.' , array('@tag' => $settings['title_wrapper']));
      }

      if ($settings['lazyload']) {
        $summary[] = t('Lazyload enabled.');
      }

      break;

    case 'penceo_countries_ioc':
      $summary[] = t('Display IOC code instead of ISO-3 for countries.');
      break;
  }

  return implode('<br />', $summary);

}

/**
 * Implements hook_field_formatter_view().
 */
function penceo_core_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $settings = $display['settings'];

  switch ($display['type']) {
    case 'penceo_cs_adaptive_image':

      // Check if the formatter involves a link.
      if ($settings['image_link'] == 'content') {
        $uri = entity_uri($entity_type, $entity);
      }
      elseif ($settings['image_link'] == 'file') {
        $link_file = TRUE;
      }
      elseif ($settings['image_link'] == 'field') {
        $link_field_items = field_get_items($entity_type, $entity, $settings['field_name']);
        $uri = array(
          'path' => $link_field_items[0]['url'],
          'options' => $link_field_items[0]['attributes'],
        );
      }

      // Check for file type.
      $is_file = $field['type'] === 'file';

      $display_settings = $settings['display_settings'];
      $start_delta = $display_settings['offset'] ? $display_settings['offset'] : 0;
      $all_items_num = count($items);

      if ($display_settings['source_field'] !== 'none') {
        try {
          $wrapper_entity = entity_metadata_wrapper($entity_type, $entity);
          $image_value = $wrapper_entity->{$display_settings['source_field']}->value();
        }
        catch (EntityMetadataWrapperException $e) {
          watchdog_exception('entity_metadata_wrapper', $e, 'entity_metadata_wrapper error in %error_loc', array('%error_loc' => __FUNCTION__ . ' @ ' . __FILE__ . ' : ' . __LINE__), WATCHDOG_CRITICAL);
          return;
        }
      }

      if ($display_settings['number_of_items'] > 0 && $all_items_num > $display_settings['number_of_items']) {
        $number_of_items_to_show = $display_settings['number_of_items'];
      }
      else {
        $number_of_items_to_show = $all_items_num;
      }

      $styles = penceo_core_field_formatter_cs_adaptive_style_normalizer($settings['styles']);

      for ($delta = $start_delta, $item_processed = 0; $delta < $all_items_num; ++$delta, ++$item_processed) {
        if ($number_of_items_to_show != $item_processed) {
          // Item piece.
          $item = !empty($image_value) ? $image_value : $items[$delta];

          // Default item.
          $image_item = $item;
          $item_uri = $item['uri'];

          // The formatter can be used on file types as well.
          if ($is_file) {
            $wrapper = file_stream_wrapper_get_instance_by_uri($item_uri);
            if ($wrapper) {
              switch (get_class($wrapper)) {
                // Supported video providers.
                case 'MediaYouTubeStreamWrapper':
                case 'MediaVimeoStreamWrapper':
                case 'MediaDailymotionStreamWrapper':
                  $file_uri = $wrapper->getLocalThumbnailPath();
                  $image_info = image_get_info($file_uri);

                  // We have to override the image item info for a file item.
                  $image_item = array(
                    'width' => $image_info['width'],
                    'height' => $image_info['height'],
                    'uri' => $file_uri,
                    'filemime' => $image_info['mime_type'],
                    'filesize' => $image_info['file_size'],
                    'alt' => !empty($item['alt']) ? $item['alt'] : '',
                    'title' => !empty($item['title']) ? $item['title'] : '',

                    'fid' => $item['fid'],
                  );
                  break;

              }
            }
          }

          if (isset($link_file)) {
            $uri = array(
              'path' => file_create_url($item_uri),
              'options' => array(),
            );
          }

          $element[$delta] = array(
            '#theme' => 'penceo_cs_adaptive_image_formatter',
            '#item' => $image_item,
            '#breakpoint_styles' => $styles,
            '#max_style' => isset($settings['styles']['max_style']) ? _cs_adaptive_image_check_image_style($settings['styles']['max_style']) : '',
            '#fallback_style' => isset($settings['styles']['fallback_style']) ? _cs_adaptive_image_check_image_style($settings['styles']['fallback_style']) : '',
            '#path' => isset($uri) ? $uri : '',
            '#class' => $settings['class'] ? explode(' ', $settings['class']) : array(),
            '#title_wrapper' => $settings['title_wrapper'],
            '#title' => entity_label($entity_type, $entity),
            '#lazyload' => $settings['display_settings']['lazyload'],
          );

          ctools_include('helpers', 'penceo_core');
          penceo_attach_library($element, 'module', 'penceo_core', 'js/jquery.lazyload/jquery.lazyload.min.js');
        }

      }

      break;

    case 'penceo_countries_ioc':
      ctools_include('helpers', 'penceo_core');
      foreach ($items as $delta => $item) {
        $element[$delta] = array(
          '#markup' => penceo_ioc_by_iso($item['country']->iso3),
        );
      }
      break;

  }

  return $element;
}

/**
 * Handler callback for penceo_cs_adaptive_image formatter.
 *
 * @return array
 *   Return available link to options.
 */
function penceo_core_field_formatter_get_link_types() {
  return array(
    'content' => t('Content'),
    'file' => t('File'),
    'field' => t('Link Field'),
  );
}

/**
 * Helper function that returns field names for the given $type.
 *
 * @param string $type
 *    The field type.
 *
 * @return array
 *   Return field names.
 */
function penceo_core_field_formatter_get_source_fields($type) {
  static $fields;

  if (!isset($fields)) {
    $fields = field_info_fields();
  }

  $selected_fields = array();
  foreach ($fields as $field_name => $field_data) {
    if ($field_data['type'] == $type) {
      $selected_fields[$field_name] = $field_name;
    }
  }

  return $selected_fields;
}

/**
 * Prepares styles for the penceo_cs_adaptive_image formatter.
 *
 * @param array $styles_raw
 *    An array of styles and breakpoint pairs returned by formatter settings.
 *
 * @return array
 *    Normalized array which can be directly passed to the formatter.
 */
function penceo_core_field_formatter_cs_adaptive_style_normalizer($styles_raw) {
  $styles = array();
  for ($i = 1; $i <= variable_get('cs_adaptive_image_breakpoint_count', 5); $i++) {
    // If breakpoint is defined.
    if (isset($styles_raw['breakpoint_' . $i]) && isset($styles_raw['style_' . $i]) && $styles_raw['breakpoint_' . $i] > 0) {
      // Associate valid style to breakpoint.
      $styles[$styles_raw['breakpoint_' . $i]] = _cs_adaptive_image_check_image_style($styles_raw['style_' . $i]);
    }
  }
  return $styles;
}
