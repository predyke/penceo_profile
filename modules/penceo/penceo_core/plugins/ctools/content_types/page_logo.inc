<?php
/**
 * @file
 * Plugin definition and callbacks for a ctools:content_types plugin.
 *
 * @ingroup CToolsPlugin CToolsPluginContentTypes
 */

$plugin = array(
  'category' => t('PENCEO'),
  'title' => t('Page logo'),
  'description' => t('Site logo that handles different images for header and footer.'),
  'single' => FALSE,
  'all contexts' => FALSE,
  'defaults' => array(
    'wrapper' => 0,
    'region' => 'header',
    'class' => '',
  ),
  'no title override' => FALSE,
  'edit form'          => 'penceo_core_ctools_content_types_page_logo_edit_form',
  'edit form validate' => 'penceo_core_ctools_content_types_page_logo_edit_form_validate',
  'edit form submit'   => 'penceo_core_ctools_content_types_page_logo_edit_form_submit',
  'admin info' => 'penceo_core_ctools_content_types_page_logo_admin_info',
  'render callback' => 'penceo_core_ctools_content_types_page_logo_render',
);

/**
 * Edit form callback.
 */
function penceo_core_ctools_content_types_page_logo_edit_form($form, &$form_state) {
  $defaults = array();
  if (isset($form_state['subtype']['defaults'])) {
    $defaults = $form_state['subtype']['defaults'];
  }
  elseif (isset($form_state['plugin']['defaults'])) {
    $defaults = $form_state['plugin']['defaults'];
  }

  $conf = $form_state['conf'] + $defaults;

  $form['wrapper'] = array(
    '#type' => 'checkbox',
    '#title' => t('Wrapper'),
    '#description' => t('When enabled the logo will be displayed as a simple anchor. Useful when the logo comes from an image sprite.'),
    '#default_value' => $conf['wrapper'],
  );

  $form['region'] = array(
    '#type' => 'select',
    '#title' => t('Region'),
    '#description' => t('Indicates which region the logo will be displayed in.'),
    '#options' => array('header' => t('Header'), 'footer' => t('Footer')),
    '#default_value' => $conf['region'],
  );

  $form['class'] = array(
    '#type' => 'textfield',
    '#title' => t('Class'),
    '#description' => t('Add additional classes to the logo anchor. Classes must be separated with space.'),
    '#default_value' => $conf['class'],
  );

  return $form;
}

/**
 * Edit form submit callback.
 */
function penceo_core_ctools_content_types_page_logo_edit_form_submit($form, &$form_state) {
  $defaults = array();
  if (isset($form_state['subtype']['defaults'])) {
    $defaults = $form_state['subtype']['defaults'];
  }
  elseif (isset($form_state['plugin']['defaults'])) {
    $defaults = $form_state['plugin']['defaults'];
  }

  foreach (array_keys($defaults) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Admin info callback.
 *
 * @param string $subtype
 *   Identifier of subtype.
 * @param array $conf
 *   Pane configuration. Array keys are depends on the _edit_form.
 *   - override_title
 *   - override_title_text
 * @param array $context
 *   An array of context objects available for use. These may be placeholders.
 *
 * @return object
 *   Administrative title and description of the $subtype pane.
 *   The keys are:
 *   - title: string
 *   - content: string|array
 */
function penceo_core_ctools_content_types_page_logo_admin_info($subtype, $conf, $context) {
  $return = new stdClass();
  $return->title = "Page logo<br>Region: {$conf['region']}<br>wrapper: {$conf['wrapper']}";
  $return->content = "This is the admin info of penceo_core:page_logo:$subtype";

  return $return;
}

/**
 * Render callback.
 *
 * "CTools:Content types" plugin render callback for "Penceo core:Page logo".
 *
 * @param string $subtype
 *   Subtype identifier.
 * @param array  $conf
 *   Configuration of the $subtype instance.
 * @param array  $args
 *   Documentation missing.
 * @param array  $pane_context
 *   Documentation missing.
 * @param array  $incoming_content
 *   Documentation missing.
 *
 * @return object
 *   The content.
 */
function penceo_core_ctools_content_types_page_logo_render($subtype, $conf, $args, $pane_context, $incoming_content) {
  $block = new stdClass();
  $block->module = 'penceo_core';
  $block->delta = $subtype;
  $block->title = '';
  $block->content = '';

  $region = isset($conf['region']) ? $conf['region'] : 'header';

  $attributes = array(
    'rel' => 'home',
    'title' => t('Home'),
    'class' => array('site-logo', $region),
  );

  if ($conf['class']) {
    $attributes['class'] = array_merge($attributes['class'], explode(' ', $conf['class']));
  }

  if (isset($conf['region']) && $conf['region']) {
    $block->content = l(t('Home'), '', array(
      'attributes' => $attributes,
    ));
  }
  else {
    $logo = theme_get_setting('logo');

    // We use different logos for the header and footer areas.
    if (!empty($logo) && $region == 'header') {
      $logo_path = $logo;
    }
    else {
      // Get the default theme path.
      $theme_path = drupal_get_path('theme', variable_get('theme_default', NULL));

      $logo_path = $theme_path . '/logo-footer.png';
    }

    $variables = array(
      'path' => $logo_path,
      'alt' => t('Home'),
    );

    $image = theme('image', $variables);

    $block->content = l($image, '', array(
      'html' => TRUE,
      'attributes' => $attributes,
    ));
  }

  return $block;
}

