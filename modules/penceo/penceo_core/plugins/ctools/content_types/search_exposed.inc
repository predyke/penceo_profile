<?php
/**
 * @file
 * Plugin definition and callbacks for a ctools:content_types plugin.
 *
 * @ingroup CToolsPlugin CToolsPluginContentTypes
 */

$plugin = array(
  'category' => t('PENCEO'),
  'title' => t('Search exposed form'),
  'description' => t('Customized views exposed form for Search.'),
  'single' => FALSE,
  'no title override' => FALSE,
  'edit form'          => 'penceo_search_ctools_content_types_search_exposed_edit_form',
  'edit form validate' => 'penceo_search_ctools_content_types_search_exposed_edit_form_validate',
  'edit form submit'   => 'penceo_search_ctools_content_types_search_exposed_edit_form_submit',
  'admin info' => 'penceo_search_ctools_content_types_search_exposed_admin_info',
  'render callback' => 'penceo_search_ctools_content_types_search_exposed_render',
  'defaults' => array(
    'view_name' => '',
    'view_display' => '',
  ),
);

/**
 * Edit form callback.
 */
function penceo_search_ctools_content_types_search_exposed_edit_form($form, &$form_state) {

  $conf = $form_state['conf'];

  $form['view_name'] = array(
    '#type' => 'textfield',
    '#title' => t('View name'),
    '#description' => t('Type in the name of the view.'),
    '#required' => TRUE,
    '#default_value' => $conf['view_name'],
  );

  $form['view_display'] = array(
    '#type' => 'textfield',
    '#title' => t('Display name'),
    '#description' => t('Type in the name of the display that belongs to the view set above.'),
    '#required' => TRUE,
    '#default_value' => $conf['view_display'],
  );

  return $form;
}

/**
 * Edit form submit callback.
 */
function penceo_search_ctools_content_types_search_exposed_edit_form_submit($form, &$form_state) {
  $defaults = array();
  if (isset($form_state['subtype']['defaults'])) {
    $defaults = $form_state['subtype']['defaults'];
  }
  elseif (isset($form_state['plugin']['defaults'])) {
    $defaults = $form_state['plugin']['defaults'];
  }

  foreach (array_keys($defaults) as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}

/**
 * Admin info callback.
 *
 * @param string $subtype
 *   Identifier of subtype.
 * @param array $conf
 *   Pane configuration. Array keys are depends on the _edit_form.
 *   - override_title
 *   - override_title_text.
 * @param array $context
 *   An array of context objects available for use. These may be placeholders.
 *
 * @return object
 *   Administrative title and description of the $subtype pane.
 *   The keys are:
 *   - title: string
 *   - content: string|array
 */
function penceo_search_ctools_content_types_search_exposed_admin_info($subtype, $conf, $context) {
  $return = new stdClass();
  $return->title = "{$conf['view_name']} - {$conf['view_display']}";
  $return->content = "This is the admin info of penceo_search:search_exposed:$subtype";
  return $return;
}

/**
 * Render callback.
 *
 * "CTools:Content types" plugin render callback for
 * "Uipm search:Search exposed".
 *
 * @param string $subtype
 *   Subtype identifier.
 * @param array $conf
 *   Configuration of the $subtype instance.
 * @param array $args
 *   Documentation missing.
 * @param array $pane_context
 *   Documentation missing.
 * @param array $incoming_content
 *   Documentation missing.
 *
 * @return object
 *   The content.
 */
function penceo_search_ctools_content_types_search_exposed_render($subtype, $conf, $args, $pane_context, $incoming_content) {

  // Indicates how many times this ctools ct got rendered.
  // With the help of this counter we can make difference between certain
  // renders.
  static $rendered = 0, $cache;

  $block = new stdClass();
  $block->content = '';
  $block->module = 'penceo_search';
  $block->delta = $subtype;
  $block->title = 'Search exposed form';

  if ($conf['view_name'] && $conf['view_display']) {
    $cache_key = "{$conf['view_name']}:{$conf['view_display']}";
    // Make sure we do not build the form again when its already built.
    if (!isset($cache[$cache_key])) {
      $view = views_get_view($conf['view_name']);

      if (!$view) {
        watchdog('penceo_search_exposed', 'Non existing view.');
        return '';
      }

      $display_id = $conf['view_display'];
      $view->set_display($display_id);
      $view->init_handlers();
      $exposed_form_state = array(
        'view' => &$view,
        'display' => &$view->display[$display_id],
        'exposed_form_plugin' => $view->display_handler->get_plugin('exposed_form'),
        'method' => 'get',
        'rerender' => TRUE,
        'no_redirect' => TRUE,
        'always_process' => TRUE,
      );

      $cache[$cache_key] = drupal_build_form('views_exposed_form', $exposed_form_state);

    }

    // Retrieve the form then alter it to our needs.
    $form = $cache[$cache_key];

    // Force valid path.
    $form['#action'] = url('search');

    // Proper form submit value.
    $form['submit']['#value'] = t('Search');

    // Class to catch it easier.
    $form['#attributes']['class'] = array('search-exposed-form');

    // Let's use the label set for the exposed element.
    if (!empty($form['#info'])) {
      $filter = reset($form['#info']);
      if (isset($filter['label'])) {
        $value = $filter['value'];
        $label = check_plain($filter['label']);

        $filter_element = &$form[$value];
        $filter_element['#attributes']['placeholder'] = $filter_element['#title'] = $label;
        $filter_element['#title_display'] = 'invisible';
      }
    }

    // Build unique ids for the form and all form elements as well.
    $form['#id'] .= '-' . $rendered;

    foreach (element_children($form) as $form_key) {
      $form[$form_key]['#id'] .= '-' . $rendered;
    }

    $block->content['#markup'] = render($form);
  }

  $rendered++;

  return $block;
}
