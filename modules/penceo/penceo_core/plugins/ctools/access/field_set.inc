<?php

/**
 * @file
 * Ctools access plugin to provide access/visiblity if two user contexts are equal.
 */

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Field set'),
  'description' => t('Access callback: TRUE - if selected field has value, FALSE - if not'),
  'callback' => 'ctools_field_set_access_check',
  'default' => array(
    'fields' => array(),
  ),
  'settings form' => 'ctools_field_set_settings',
  'summary' => 'ctools_field_set_access_summary',
  'required context' => array(
    new ctools_context_required(t('Node'), 'node'),
  ),
);

/**
 * Settings form for the 'by perm' access plugin
 */
function ctools_field_set_settings($form, &$form_state, $conf) {
  $form['settings']['helptext'] = array(
    '#theme' => 'html_tag',
    '#tag' => 'div',
    '#attributes' => array(
      'class' => array(
        'help-text',
      ),
    ),
    '#value' => t('Access callback: TRUE - if selected field has value, FALSE - if not.'),
  );

  $form['settings']['fields'] = array(
    '#type' => 'select',
    '#title' => t('Select field'),
    '#options' => _ctools_get_fields_option_array(),
    '#default_value' => $conf['fields'],
  );
  return $form;
}

/**
 * Check for access.
 */
function ctools_field_set_access_check($conf, $context) {
  $context_node = $context[0]->data;
  $field = $conf['fields'];
  if (isset($context_node->$field) && !empty($context_node->$field)) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Describe an instance of this plugin.
 */
function ctools_field_set_access_summary($conf, $context) {
  return t('Selected field: @field', array('@field' => $conf['fields']));
}

/**
 * Helper function to get field options array.
 */
function _ctools_get_fields_option_array() {
  $output = array();
  $fields = field_info_fields();
  foreach ($fields as $key => $field) {
    $output[$key] = $field['field_name'];
  }
  return $output;
}
