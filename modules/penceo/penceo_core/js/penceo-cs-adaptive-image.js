/**
 * @file
 * JavaScript functions for the Client-side adaptive image module.
 *
 * Penceo altered version.
 */

(function ($) {
    Drupal.behaviors.csAdaptiveImage = {
        attach: function(context, settings) {
            // if (window.csAdaptiveImageInitialized) {
            //     return;
            // }
            //
            // window.csAdaptiveImageInitialized = true;

            var isLazy = true;

            if (isLazy && typeof $.fn.lazyload === 'undefined') {
                isLazy = false;
            }

            var getViewport = function() {
                var viewPortWidth;
                var viewPortHeight;

                // the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
                if (typeof window.innerWidth !== 'undefined') {
                    viewPortWidth = window.innerWidth;
                    viewPortHeight = window.innerHeight;
                }

                // IE6 in standards compliant mode (i.e. with a valid doctype as the first line in the document)
                else if (typeof document.documentElement !== 'undefined'
                    && typeof document.documentElement.clientWidth !== 'undefined'
                    && document.documentElement.clientWidth !== 0) {
                    viewPortWidth = document.documentElement.clientWidth;
                    viewPortHeight = document.documentElement.clientHeight;
                }

                // older versions of IE
                else {
                    viewPortWidth = document.getElementsByTagName('body')[0].clientWidth;
                    viewPortHeight = document.getElementsByTagName('body')[0].clientHeight;
                }
                return {
                    width: viewPortWidth,
                    height: viewPortHeight
                };
            };

            /**
             * Retrieves an adapted image based element's data attributes
             * and the current client width.
             */
            var getAdaptedImage = function(element, excluded_breakpoint) {
                var selected_breakpoint = 'max';
                var breakpoints = $(element).attr('data-adaptive-image-breakpoints');
                if (breakpoints) {
                    // Find applicable target resolution.
                    $.each(breakpoints.split(' '), function(key, breakpoint) {
                        if (getViewport().width < Number(breakpoint) && (selected_breakpoint === 'max' || Number(breakpoint) < Number(selected_breakpoint))) {
                            selected_breakpoint = breakpoint;
                        }
                    });
                }
                if (selected_breakpoint !== excluded_breakpoint) {
                    return $(element).attr('data-adaptive-image-' + selected_breakpoint + '-img');
                }
                else {
                    return false;
                }
            };

            var lazyLoad = function(wrapper) {
                var img = wrapper.next('img');

                if (img.length !== 0 && img.attr('data-original') !== 'undefined') {
                    if (isLazy) {
                        img.lazyload(
                            {
                                event : 'scroll sporty'
                            }
                        );
                    }
                    else {
                        img.attr('src', img.attr('data-original'));
                    }
                }
            };

            // Insert adapted images.
            $('noscript.adaptive-image', context).once('adaptive-image', function() {
                var img = getAdaptedImage(this);
                $(this).after(img);
                Drupal.attachBehaviors(img);
                jQuery(document).trigger('adaptiveImageInserted', [$(this).next('img')]);

                lazyLoad($(this));
            });

            // Replace adapted images on window resize.
            jQuery(window)
                .off('resizeend.onAdaptiveImageResizeend')
                .on('resizeend.onAdaptiveImageResizeend', function() {
                    $('noscript.adaptive-image-processed').each(function() {
                        // Replace image if it does not match the same breakpoint.
                        var excluded_breakpoint = $(this).next('img.adaptive-image').attr('data-adaptive-image-breakpoint');
                        var img = getAdaptedImage(this, excluded_breakpoint);

                        if (img) {
                            $(this).next('img.adaptive-image').replaceWith(img);
                            Drupal.attachBehaviors(img);
                            jQuery(document).trigger('adaptiveImageInserted', [$(this).next('img')]);

                            lazyLoad($(this));
                        }
                    });
                });
        }
    };
})(jQuery);