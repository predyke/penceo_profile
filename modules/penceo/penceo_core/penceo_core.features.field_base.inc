<?php
/**
 * @file
 * penceo_core.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function penceo_core_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_folder'.
  $field_bases['field_folder'] = array(
    'active' => 1,
    'bundle' => 'image',
    'cardinality' => 1,
    'deleted' => 0,
    'entity_type' => 'file',
    'entity_types' => array(),
    'field_name' => 'field_folder',
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'label' => 'Media Folder',
    'locked' => 0,
    'module' => 'taxonomy',
    'required' => TRUE,
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'media_folders',
          'parent' => 0,
        ),
      ),
      'options_list_callback' => 'title_taxonomy_allowed_values',
    ),
    'translatable' => 0,
    'type' => 'taxonomy_term_reference',
  );

  // Exported field_base: 'title_field'.
  $field_bases['title_field'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'title_field',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 1,
    'type' => 'text',
  );

  return $field_bases;
}
