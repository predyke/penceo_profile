<?php
/**
 * @file
 * Helper functions storage for penceo_profile.
 */

/**
 * Helper function to generate taxonomy terms.
 *
 * @param array $elements
 *    Keyed by vocabulary.
 *
 * @return array
 *    The return value is an array of term ids as key with true false values
 *    which indicate whether the given term was created now or already existed.
 *    Empty array in case of non existing vocabulary.
 */
function penceo_terms_generate($elements = array()) {
  $success = array();

  foreach ($elements as $vocabulary => $terms) {
    $vocab = taxonomy_vocabulary_machine_name_load($vocabulary);

    foreach ($terms as $term_name) {
      // Check whether the term exists or not.
      $stored = taxonomy_get_term_by_name($term_name, $vocabulary);
      if (!empty($stored)) {
        $success[$vocabulary][] = FALSE;
      }
      else {
        $term = new stdClass();
        $term->name = $term_name;
        $term->vid = $vocab->vid;
        $success[$vocabulary][$term->tid] = taxonomy_term_save($term);
      }
    }
  }
  return $success;
}

/**
 * Helper function to get taxonomy term options for the given vocabulary.
 *
 * @param string $machine_name
 *   Vocabulary machine name.
 * @param $parent
 *   The term ID under which to generate the tree. If 0, generate the tree
 *   for the entire vocabulary.
 * @param $max_depth
 *   The number of levels of the tree to return. Leave NULL to return all levels.
 * @param $load_entities
 *   If TRUE, a full entity load will occur on the term objects. Otherwise they
 *   are partial objects queried directly from the {taxonomy_term_data} table to
 *   save execution time and memory consumption when listing large numbers of
 *   terms. Defaults to FALSE.
 *
 * @return array
 *   Taxonomy terms keyed by the taxonomy term tid.
 */
function penceo_get_taxonomy_term_options($machine_name, $parent = 0, $max_depth = NULL, $load_entities = FALSE) {
  $vocab = taxonomy_vocabulary_machine_name_load($machine_name);
  $options_source = taxonomy_get_tree($vocab->vid, $parent, $max_depth, $load_entities);
  $options = array();

  foreach($options_source as $item) {
    $options[$item->tid] = !$load_entities ? $item->name : $item;
  }

  return $options;
}

/**
 * Checks whether the current user is uid == 1 or high level administrator.
 *
 * @param object $user
 *    If not set user is current logged in user.
 *
 * @return bool
 *    True if given user belongs to the administrator role or UID == 1.
 */
function penceo_is_admin($user = NULL) {
  if (!isset($user)) {
    global $user;
  }
  return $user->uid == 1 || in_array('administrator', $user->roles);
}

/**
 * Checks if user is site administrator or content manager.
 *
 * @param object $user
 *    If not set user is current logged in user.
 *
 * @return bool
 *    True if given user belongs to site administrator or content manager role.
 */
function penceo_is_client_admin($user = NULL) {
  if (!isset($user)) {
    global $user;
  }
  return in_array('content manager', $user->roles) || in_array('site administrator', $user->roles);
}

/**
 * Checks whether the given user is a simple authenticated user and nothing else.
 *
 * @param object $user
 *    If not set user is current logged in user.
 *
 * @return bool
 *    True when the user does not belong to any roles other than authenticated.
 */
function penceo_is_only_authenticated($user = NULL) {
  if (!isset($user)) {
    global $user;
  }

  return (bool)!($user->uid < 2 || 1 < count($user->roles));
}

/**
 * Helper function to delete field instances.
 *
 * @param array $instances
 *    An array of instances with the following required keys:
 *   - entity_type
 *   - field_name
 *   - bundle_name.
 * @param bool $cleanup
 *    When true immediately removes the given instances.
 *
 * @return bool
 *    True on success.
 */
function penceo_field_instance_delete($instances = array(), $cleanup = TRUE) {
  foreach ($instances as $instance_info) {
    if (!(isset($instance_info['entity_type'])
      && isset($instance_info['field_name'])
      && isset($instance_info['bundle_name']))
    ) {
      return FALSE;
    }
    if ($instance = field_info_instance(
      $instance_info['entity_type'],
      $instance_info['field_name'],
      $instance_info['bundle_name'])
    ) {
      field_delete_instance($instance, $cleanup);
    }
  }
  if ($cleanup) {
    // Immediately delete.
    field_purge_batch(1);
  }

  return TRUE;
}

/**
 * Returns ioc code of a country by iso2.
 *
 * @param string $iso
 *    The iso code of the country.
 *
 * @return string $ioc
 *    The ioc code of the country.
 */
function penceo_ioc_by_iso($iso) {
  $codes = variable_get('penceo_core_country_code_table');

  if (empty($codes)) {
    $codes = penceo_country_table();
    variable_set('penceo_core_country_code_table', $codes);
  }

  foreach ($codes as $code) {
    if (isset($code[4]) && $code[4] == $iso) {
      return $code[2];
    }
  }
  return FALSE;
}

/**
 * Returns ioc code of a country by iso2.
 *
 * @param string $ioc
 *    The ioc code of the country.
 *
 * @return string $iso
 *    The iso code of the country.
 */
function penceo_iso_by_ioc($ioc) {
  $codes = variable_get('penceo_core_country_code_table');

  if (empty($codes)) {
    $codes = penceo_country_table();
    variable_set('penceo_core_country_code_table', $codes);
  }

  foreach ($codes as $code) {
    if (isset($code[2]) && $code[2] == $ioc) {
      return $code[4];
    }
  }
  return FALSE;
}

/**
 * Returns an array of countries.
 *
 * With country name, official name, iso3, ioc columns.
 *
 * @return array
 *   An array of countries.
 */
function penceo_country_table() {
  $code_list = "Afghanistan	Afghanistan	AFG	AFG	AFG
Ã…land Islands	Ã…land Islands			ALA
Albania	Albania	ALB	ALB	ALB
Algeria	Algeria	ALG	ALG	DZA
American Samoa	American Samoa[1]	ASA	ASA	ASM
Andorra	Andorra	AND	AND	AND
Angola	Angola	ANG	ANG	AGO
Anguilla	Anguilla		AIA	AIA
Antarctica			ATA
Antigua and Barbuda	Antigua and Barbuda	ANT	ATG	ATG
Argentina	Argentina	ARG	ARG	ARG
Armenia	Armenia	ARM	ARM	ARM
Aruba	Aruba	ARU	ARU	ABW
Australia	Australia	AUS	AUS	AUS
Austria	Austria	AUT	AUT	AUT
Azerbaijan	Azerbaijan	AZE	AZE	AZE
The Bahamas	The Bahamas	BAH	BAH	BHS
Bahrain	Bahrain	BRN	BHR	BHR
Bangladesh	Bangladesh	BAN	BAN	BGD
Barbados	Barbados	BAR	BRB	BRB
Belarus	Belarus	BLR	BLR	BLR
Belgium	Belgium	BEL	BEL	BEL
Belize	Belize	BIZ	BLZ	BLZ
Benin	Benin	BEN	BEN	BEN
Bermuda	Bermuda	BER	BER	BMU
Bhutan	Bhutan	BHU	BHU	BTN
Bolivia	Bolivia	BOL	BOL	BOL
Caribbean Netherlands	Caribbean Netherlands: Bonaire, Sint Eustatius and Saba			BES
Bosnia and Herzegovina	Bosnia and Herzegovina	BIH	BIH	BIH
Botswana	Botswana	BOT	BOT	BWA
Norway	Bouvet Island			BVT
Brazil	Brazil	BRA	BRA	BRA
British Indian Ocean Territory	British Indian Ocean Territory			IOT
British Virgin Islands	British Virgin Islands[2]	IVB	VGB	VGB
Brunei	Brunei	BRU	BRU	BRN
Bulgaria	Bulgaria	BUL	BUL	BGR
Burkina Faso	Burkina Faso	BUR	BFA	BFA
Burundi	Burundi	BDI	BDI	BDI
Cambodia	Cambodia	CAM	CAM	KHM
Cameroon	Cameroon	CMR	CMR	CMR
Canada	Canada	CAN	CAN	CAN
Cape Verde	Cape Verde	CPV	CPV	CPV
Cayman Islands	Cayman Islands	CAY	CAY	CYM
Central African Republic	Central African Republic	CAF	CTA	CAF
Chad	Chad	CHA	CHA	TCD
Chile	Chile	CHI	CHI	CHL
China	China, People's Republic of[3]	CHN	CHN	CHN
Christmas Island	Christmas Island			CXR
Cocos (Keeling) Islands	Cocos (Keeling) Islands			CCK
Colombia	Colombia	COL	COL	COL
Comoros	Comoros	COM	COM	COM
Democratic Republic of the Congo	Congo, Democratic Republic of the[4]	COD	COD	COD
Republic of the Congo	Congo, Republic of the[5]	CGO	CGO	COG
Cook Islands	Cook Islands	COK	COK	COK
Costa Rica	Costa Rica	CRC	CRC	CRI
Ivory Coast	CÃ´te d'Ivoire	CIV	CIV	CIV
Croatia	Croatia	CRO	CRO	HRV
Cuba	Cuba	CUB	CUB	CUB
CuraÃ§ao	CuraÃ§ao		CUW	CUW
Cyprus	Cyprus	CYP	CYP	CYP
Czech Republic	Czech Republic	CZE	CZE	CZE
Denmark	Denmark	DEN	DEN	DNK
Djibouti	Djibouti	DJI	DJI	DJI
Dominica	Dominica	DMA	DMA	DMA
Dominican Republic	Dominican Republic	DOM	DOM	DOM
Ecuador	Ecuador	ECU	ECU	ECU
Egypt	Egypt	EGY	EGY	EGY
El Salvador	El Salvador	ESA	SLV	SLV
England	England		ENG	[6]
Equatorial Guinea	Equatorial Guinea	GEQ	EQG	GNQ
Eritrea	Eritrea	ERI	ERI	ERI
Estonia	Estonia	EST	EST	EST
Ethiopia	Ethiopia	ETH	ETH	ETH
Falkland Islands	Falkland Islands			FLK
Faroe Islands	Faroe Islands		FRO	FRO
Fiji	Fiji	FIJ	FIJ	FJI
Finland	Finland	FIN	FIN	FIN
France	France	FRA	FRA	FRA
French Guiana	French Guiana			GUF
French Polynesia	French Polynesia[7]		TAH	PYF
French Southern and Antarctic Lands	French Southern and Antarctic Lands			ATF
Gabon	Gabon	GAB	GAB	GAB
The Gambia	The Gambia	GAM	GAM	GMB
Georgia (country)	Georgia	GEO	GEO	GEO
Germany	Germany	GER	GER	DEU
Ghana	Ghana	GHA	GHA	GHA
Gibraltar	Gibraltar			GIB
Greece	Greece	GRE	GRE	GRC
Greenland	Greenland			GRL
Grenada	Grenada	GRN	GRN	GRD
Guadeloupe	Guadeloupe			GLP
Guam	Guam	GUM	GUM	GUM
Guatemala	Guatemala	GUA	GUA	GTM
Guernsey	Guernsey			GGY
Guinea	Guinea	GUI	GUI	GIN
Guinea-Bissau	Guinea-Bissau	GBS	GNB	GNB
Guyana	Guyana	GUY	GUY	GUY
Haiti	Haiti	HAI	HAI	HTI
Australia	Heard Island and McDonald Islands			HMD
Honduras	Honduras	HON	HON	HND
Hong Kong	Hong Kong[8]	HKG	HKG	HKG
Hungary	Hungary	HUN	HUN	HUN
Iceland	Iceland	ISL	ISL	ISL
India	India	IND	IND	IND
Indonesia	Indonesia	INA	IDN	IDN
Iran	Iran	IRI	IRN	IRN
Iraq	Iraq	IRQ	IRQ	IRQ
Republic of Ireland	Ireland[9]	IRL	IRL	IRL
Isle of Man	Isle of Man			IMN
Israel	Israel	ISR	ISR	ISR
Italy	Italy	ITA	ITA	ITA
Jamaica	Jamaica	JAM	JAM	JAM
Japan	Japan	JPN	JPN	JPN
Jersey	Jersey			JEY
Jordan	Jordan	JOR	JOR	JOR
Kazakhstan	Kazakhstan	KAZ	KAZ	KAZ
Kenya	Kenya	KEN	KEN	KEN
Kiribati	Kiribati	KIR		KIR
North Korea	Korea, Democratic People's Rep. (North)[10]	PRK	PRK	PRK
South Korea	Korea, Republic of (South)[11]	KOR	KOR	KOR
Kuwait	Kuwait	KUW	KUW	KWT
Kyrgyzstan	Kyrgyzstan	KGZ	KGZ	KGZ
Laos	Laos	LAO	LAO	LAO
Latvia	Latvia	LAT	LVA	LVA
Lebanon	Lebanon	LIB	LIB	LBN
Lesotho	Lesotho	LES	LES	LSO
Liberia	Liberia	LBR	LBR	LBR
Libya	Libya	LBA	LBY	LBY
Liechtenstein	Liechtenstein	LIE	LIE	LIE
Lithuania	Lithuania	LTU	LTU	LTU
Luxembourg	Luxembourg	LUX	LUX	LUX
Macau	Macau[12]		MAC	MAC
Republic of Macedonia	Macedonia[13]	MKD	MKD	MKD
Madagascar	Madagascar	MAD	MAD	MDG
Malawi	Malawi	MAW	MWI	MWI
Malaysia	Malaysia	MAS	MAS	MYS
Maldives	Maldives	MDV	MDV	MDV
Mali	Mali	MLI	MLI	MLI
Malta	Malta	MLT	MLT	MLT
Marshall Islands	Marshall Islands	MHL		MHL
Martinique	Martinique			MTQ
Mauritania	Mauritania	MTN	MTN	MRT
Mauritius	Mauritius	MRI	MRI	MUS
Mayotte	Mayotte			MYT
Mexico	Mexico	MEX	MEX	MEX
Federated States of Micronesia	Micronesia, Federated States of	FSM		FSM
Moldova	Moldova	MDA	MDA	MDA
Monaco	Monaco	MON		MCO
Mongolia	Mongolia	MGL	MGL	MNG
Montenegro	Montenegro	MNE	MNE	MNE
Montserrat	Montserrat		MSR	MSR
Morocco	Morocco	MAR	MAR	MAR
Mozambique	Mozambique	MOZ	MOZ	MOZ
Myanmar	Myanmar	MYA	MYA	MMR
Namibia	Namibia	NAM	NAM	NAM
Nauru	Nauru	NRU		NRU
Nepal	Nepal	NEP	NEP	NPL
Netherlands	Netherlands	NED	NED	NLD
New Caledonia	New Caledonia		NCL	NCL
New Zealand	New Zealand	NZL	NZL	NZL
Nicaragua	Nicaragua	NCA	NCA	NIC
Niger	Niger	NIG	NIG	NER
Nigeria	Nigeria	NGR	NGA	NGA
Niue	Niue			NIU
Norfolk Island	Norfolk Island			NFK
Northern Ireland	Northern Ireland		NIR	[6]
Northern Mariana Islands	Northern Mariana Islands			MNP
Norway	Norway	NOR	NOR	NOR
Oman	Oman	OMA	OMA	OMN
Pakistan	Pakistan	PAK	PAK	PAK
Palau	Palau	PLW		PLW
Palestinian territories	State of Palestine	PLE	PLE	PSE
Panama	Panama	PAN	PAN	PAN
Papua New Guinea	Papua New Guinea	PNG	PNG	PNG
Paraguay	Paraguay	PAR	PAR	PRY
Peru	Peru	PER	PER	PER
Philippines	Philippines	PHI	PHI	PHL
Pitcairn Islands	Pitcairn Islands			PCN
Poland	Poland	POL	POL	POL
Portugal	Portugal	POR	POR	PRT
Puerto Rico	Puerto Rico	PUR	PUR	PRI
Qatar	Qatar	QAT	QAT	QAT
RÃ©union	RÃ©union			REU
Romania	Romania	ROU	ROU	ROU
Russia	Russia	RUS	RUS	RUS
Rwanda	Rwanda	RWA	RWA	RWA
Saint BarthÃ©lemy	Saint BarthÃ©lemy			BLM
Saint Helena	Saint Helena, Ascension and Tristan da Cunha			SHN
Saint Kitts and Nevis	Saint Kitts and Nevis	SKN	SKN	KNA
Saint Lucia	Saint Lucia	LCA	LCA	LCA
Collectivity of Saint Martin	Saint Martin (French part)			MAF
Saint Pierre and Miquelon	Saint Pierre and Miquelon			SPM
Saint Vincent and the Grenadines	Saint Vincent and the Grenadines	VIN	VIN	VCT
Samoa	Samoa[1]	SAM	SAM	WSM
San Marino	San Marino	SMR	SMR	SMR
SÃ£o TomÃ© and PrÃ­ncipe	SÃ£o TomÃ© and PrÃ­ncipe	STP	STP	STP
Saudi Arabia	Saudi Arabia	KSA	KSA	SAU
Scotland	Scotland		SCO	[6]
Senegal	Senegal	SEN	SEN	SEN
Serbia	Serbia	SRB	SRB	SRB
Seychelles	Seychelles	SEY	SEY	SYC
Sierra Leone	Sierra Leone	SLE	SLE	SLE
Singapore	Singapore	SIN	SIN	SGP
Sint Maarten	Sint Maarten (Dutch part)			SXM
Slovakia	Slovakia	SVK	SVK	SVK
Slovenia	Slovenia	SLO	SVN	SVN
Solomon Islands	Solomon Islands	SOL	SOL	SLB
Somalia	Somalia	SOM	SOM	SOM
South Africa	South Africa	RSA	RSA	ZAF
South Georgia and the South Sandwich Islands	South Georgia and the South Sandwich Islands			SGS
South Sudan	South Sudan		SSD	SSD
Spain	Spain	ESP	ESP	ESP
Sri Lanka	Sri Lanka	SRI	SRI	LKA
Sudan	Sudan	SUD	SDN	SDN
Suriname	Suriname	SUR	SUR	SUR
Norway	Svalbard and Jan Mayen			SJM
Swaziland	Swaziland	SWZ	SWZ	SWZ
Sweden	Sweden	SWE	SWE	SWE
Switzerland	Switzerland	SUI	SUI	CHE
Syria	Syria	SYR	SYR	SYR
Taiwan	Republic of China (Taiwan)[14]	TPE	TPE	TWN
Tajikistan	Tajikistan	TJK	TJK	TJK
Tanzania	Tanzania	TAN	TAN	TZA
Thailand	Thailand	THA	THA	THA
East Timor	Timor-Leste[15]	TLS	TLS	TLS
Togo	Togo	TOG	TOG	TGO
Tokelau	Tokelau			TKL
Tonga	Tonga	TGA	TGA	TON
Trinidad and Tobago	Trinidad and Tobago	TRI	TRI	TTO
Tunisia	Tunisia	TUN	TUN	TUN
Turkey	Turkey	TUR	TUR	TUR
Turkmenistan	Turkmenistan	TKM	TKM	TKM
Turks and Caicos Islands	Turks and Caicos Islands		TCA	TCA
Tuvalu	Tuvalu	TUV		TUV
Uganda	Uganda	UGA	UGA	UGA
Ukraine	Ukraine	UKR	UKR	UKR
United Arab Emirates	United Arab Emirates	UAE	UAE	ARE
United Kingdom	United Kingdom[16]	GBR		GBR
United States	United States	USA	USA	USA
United States	United States Minor Outlying Islands			UMI
United States Virgin Islands	United States Virgin Islands[17]	ISV	VIR	VIR
Uruguay	Uruguay	URU	URU	URY
Uzbekistan	Uzbekistan	UZB	UZB	UZB
Vanuatu	Vanuatu	VAN	VAN	VUT
Vatican City	Vatican City State			VAT
Venezuela	Venezuela	VEN	VEN	VEN
Vietnam	Vietnam	VIE	VIE	VNM
Wales	Wales		WAL	[6]
Wallis and Futuna	Wallis and Futuna			WLF
Western Sahara			ESH
Yemen	Yemen	YEM	YEM	YEM
Zambia	Zambia	ZAM	ZAM	ZMB
Zimbabwe	Zimbabwe	ZIM	ZIM	ZWE
Kosovo, Republic of	Republic of Kosovo	KOS	UNK	UNK";

  $countries = explode("\n", $code_list);
  $codes = array();
  foreach ($countries as $country) {
    $codes[] = explode("\t", $country);
  }

  return $codes;
}

/**
 * Helper function that attaches external libraries.
 *
 * Such as js or css to the given content.
 *
 * @param array &$content
 *    The content which the given library should be attached to.
 *    e.g.:$block->content, $form etc.
 * @param string $path_type
 *    This will be directly passed to drupal_get_path
 *    options are:
 *    module
 *    theme
 *    profile
 *    theme_engine.
 * @param string $name
 *    This will be directly passed to drupal_get_path
 *    name of the module or theme.
 * @param string $path
 *    The path to the file that should be attached
 *    no trailing slash needed.
 * @param string $library
 *    Either js or css.
 */
function penceo_attach_library(&$content, $path_type, $name, $path, $library = 'js') {
  static $cache;

  if (!isset($cache)) {
    $cache['added'] = &drupal_static(__FUNCTION__, FALSE);
  }

  $added = &$cache['added'];

  if (!isset($added[$path])) {
    $added[$path] = drupal_get_path($path_type, $name) . '/' . ltrim($path, '/');
    $content['#attached'][$library][] = $added[$path];
  }
}

/**
 * This helper returns an array of possible html tags.
 *
 * @param string $type
 *    The type of html tags to return.
 *    Possible values:
 *      semantic: article, section etc.
 *      non-semantic: span, div
 *
 * Most likely used for panels display-edit render.
 *
 * @return array
 *    Returns an array of available html tags.
 */
function penceo_tags($type = NULL) {

  $tags['non-semantic'] = array(
    'div' => t('DIV'),
    'span' => t('SPAN'),
  );

  $tags['semantic'] = array(
    'article' => t('ARTICLE'),
    'section' => t('SECTION'),
    'aside' => t('ASIDE'),
    'details' => t('DETAILS'),
    'figure' => t('FIGURE'),
    'figcaption' => t('FIGCAPTION'),
    'main' => t('MAIN'),
    'header' => t('HEADER'),
    'footer' => t('FOOTER'),
    'mark' => t('MARK'),
    'nav' => t('NAV'),
    'summary' => t('SUMMARY'),
    'time' => t('TIME'),
    'p' => t('P'),
  );

  return isset($tags[$type]) ? $tags[$type] : array_merge($tags['non-semantic'], $tags['semantic']);
}

/**
 * Slices ap data array and processes with the given $batch_size.
 *
 * Useful when pulling loads of data from external sources.
 *
 * @param array $data
 *    The array of data that should be processed.
 * @param string $callback
 *    The name of the callback function that will do the processing for a batch.
 * @param int $batch_size
 *    The number of items to process in one run.
 * @param int $limit
 *    If set only the given amount of $data will be processed.
 * @param int $return
 *    When set to the true and the callback function's return value
 *    will be gathered in an array then returned.
 * @param array $memory_peaks
 *    A optional reference element that can hold the memory peaks of each batch.
 *    For testing or diagnostic reasons.
 *
 * @return array
 *    If $return is true an array of data will be returned.
 */
function penceo_process_batch(array $data, $callback, $batch_size = 10, $limit = NULL, $return = FALSE, &$memory_peaks = array()) {
  $return = array();

  for ($leap = 0; $leap < ($max = isset($limit) && $limit > 0 ? $limit : count($data)); $leap += $batch_size) {
    $distance = ($remainder = $max - $leap) > 0 && $remainder < $batch_size ? $remainder : $batch_size;
    $pieces = array_slice($data, $leap, $distance, TRUE);
    if ($return) {
      $return[] = $callback($pieces);
    }
    else {
      $callback($pieces);
    }
    $memory_peaks[] = memory_get_peak_usage();
  }

  // In case there is a callback value we must return it.
  if (!empty($return)) {
    return $return;
  }
}

/**
 * Helper function to get the term_id of the media folder term by path.
 * Separate path names with a slash, example: Athlete/Photo/Profile.
 *
 * @param string $term_path
 *    The path of the term.
 *
 * @return mixed
 *    The taxonomy term object if exists otherwise false.
 */
function penceo_get_media_folder_term($term_path) {
  $vocabulary = taxonomy_vocabulary_machine_name_load('media_folders');

  $path_items = explode("/", $term_path);
  $path_items = array_reverse($path_items);

  $query = db_select('taxonomy_term_data', 'ttd0');
  $query->join('taxonomy_term_hierarchy', 'tth0', "tth0.tid = ttd0.tid");
  $query->fields('ttd0', array('tid'));
  $query->condition('ttd0.name', $path_items[0]);
  $query->condition('ttd0.vid', $vocabulary->vid);

  for ($i = 1 ; $i < count($path_items) ; $i++) {
    $alias_current_hierarchy = "tth" . $i;
    $alias_parent_hierarchy = "tth" . ($i-1);

    $alias_current_data = "ttd" . $i;

    $query->join('taxonomy_term_data', $alias_current_data, "{$alias_current_data}.tid = {$alias_parent_hierarchy}.parent");
    $query->join('taxonomy_term_hierarchy', $alias_current_hierarchy, "{$alias_current_hierarchy}.tid = {$alias_current_data}.tid");

    $query->condition("{$alias_current_data}.name", $path_items[$i]);
  }

  $result = $query->execute()->fetchColumn();

  if ($result) {
    return taxonomy_term_load($result);
  }
  else {
    watchdog('php', "Cannot find term with path it media folders: @path",
      array(
        '@path' => $term_path,
      ), WATCHDOG_WARNING
    );
    return FALSE;
  }
}

/**
 * Find the given $substring in $array then returns the value.
 *
 * @param array $array
 * @param string $substring
 *
 * @return mixed
 *    The value when matched false otherwise.
 */
function penceo_in_array_substr($array, $substring) {
  foreach ($array as $value) {
    if (strpos($value, $substring) !== FALSE) {
      return $value;
    }
  }
  return FALSE;
}

/**
 * Helper function that searches for the given key or value in a
 * multidimensional array and returns the corresponding value.
 *
 * Also returns the path where the desired search value was found.
 *
 * @param array $array
 *    The array we search for the values in.
 * @param string $type
 *    [key, value] Indicates what should be checked.
 * @param mixed $search
 *    The value or key we look for.
 * @param bool $firstMatch
 *    When true it always returns the first match. When false all results
 *    will be returned.
 * @param bool $returnValue
 *    When true the found key will also return the value.
 * @return mixed $value
 *    Returns the value for the given key if Found. False otherwise.
 */
function penceo_array_search_recursive(array $array, $type, $search, $firstMatch = TRUE, $returnValue = FALSE) {
  // Create a recursive iterator to loop over the array recursively.
  $iterator = new \RecursiveIteratorIterator(
    new \RecursiveArrayIterator($array),
    \RecursiveIteratorIterator::SELF_FIRST);

  $matches = array();

  // Loop over the iterator.
  foreach ($iterator as $key => $value) {
    // If the key matches our search.
    if ("{${$type}}" === $search) {
      // Add the current key.
      $keys = array($key);
      // Loop up the recursive chain.
      for ($i = $iterator->getDepth() -1; $i >= 0; $i--) {
        // Add each parent key.
        array_unshift($keys, $iterator->getSubIterator($i)->key());
      }

      $match = array('path' => $keys, 'key' => $key);

      if ($returnValue && $firstMatch)  {
        return $value;
      }

      if ($firstMatch) {
        return $match;
      }
      else {
        $matches[] = $match;
      }
    }
  }

  return !empty($matches) ? $matches : FALSE;
}
