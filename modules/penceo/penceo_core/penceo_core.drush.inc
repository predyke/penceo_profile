<?php
/**
 * @file
 *
 * Stores drush commands for penceo_core
 */


/**
 * Implementation of hook_drush_command().
 */
function penceo_core_drush_command() {
  $items = array();
  $items['refresh-country-table'] = array(
    'callback' => '_penceo_core_refresh_country_table_command',
    'description' => 'Drush command to refresh the country table for IOC <=> ISO mapping.',
    'aliases' => array('rct'),
    'examples' => array(
      'Update all countries' => 'drush rct',
    ),
  );

  return $items;
}

/**
 * Callback function for the refresh-country-table command.
 */
function _penceo_core_refresh_country_table_command() {
  ctools_include('helpers', 'penceo_core');
  variable_set('penceo_core_country_code_table', penceo_country_table());
  drush_print('Country table has been successfully updated');
}