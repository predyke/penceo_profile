<?php
/**
 * @file
 * penceo_core.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function penceo_core_taxonomy_default_vocabularies() {
  return array(
    'media_folders' => array(
      'name' => 'Media Folders',
      'machine_name' => 'media_folders',
      'description' => 'Use media folders to organize your media',
      'hierarchy' => 1,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
