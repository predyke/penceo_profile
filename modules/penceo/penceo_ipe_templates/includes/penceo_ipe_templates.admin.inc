<?php
/**
 * @file
 * Admin form holder for penceo IPE templates.
 */

/**
 * Default form builder for pane selection.
 */
function penceo_ipe_templates_admin_form($form, &$form_state) {

  ctools_include('penceo_ipe_templates.helpers', 'penceo_ipe_templates');

  $form['pane_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pane types'),
    '#description' => t('Default pane list settings.'),
  );

  $form['pane_types']['penceo_ipe_templates_pane_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Pane types'),
    '#options' => penceo_ipe_templates_pane_type_defaults(),
    '#default_value' => variable_get('penceo_ipe_templates_pane_types', array('custom')),
    '#description' => t('The list of default pane types that will be enabled in the pane list.'),
    '#tree' => TRUE,
  );

  $form['pane_types']['penceo_ipe_templates_project_category'] = array(
    '#type' => 'textfield',
    '#title' => t('Project category'),
    '#default_value' => variable_get('penceo_ipe_templates_project_category', ''),
    '#description' => t('The category of a custom panel pane. <strong>NOTE:</strong><i>Multiple categories can be given, comma separated</i>.'),
    '#states' => array(
      'visible' => array(
        ':input[name="penceo_ipe_templates_pane_types[custom]"]' => array('checked' => TRUE),
      ),
    ),
  );

  return system_settings_form($form);
}

/**
 * Default validate function for pane selection form.
 */
function penceo_ipe_templates_admin_form_validate($form, &$form_state) {
  // Whenever the checkboxes form element receives a key found in $options
  // it will automatically check it and we do not want that.
  // In order to solve this the NULL defined elements must be removed from
  // selected.
  foreach($form_state['input']['penceo_ipe_templates_pane_types'] as $key => $type) {
    if (NULL === $type) {
      unset($form_state['input']['penceo_ipe_templates_pane_types'][$key]);
      // Remove it from values as well.
      unset($form_state['values']['penceo_ipe_templates_pane_types'][$key]);
    }
  }
}

/**
 * Default form builder.
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 *
 */
function penceo_ipe_templates_profile_form($form, &$form_state) {

  ctools_include('penceo_ipe_templates.helpers', 'penceo_ipe_templates');

  $form['profile'] = array(
    '#title' => t('Add new profile'),
    '#type' => 'fieldset',
    '#weight' => -10,
  );

  $form['profile_list'] = array(
    '#title' => t('Stored profiles'),
    '#description' => t('In the list above you can see the available profiles stored. <b>NOTE:</b> When a profile is unchecked upon save it will be completly removed!'),
    '#type' => 'fieldset',
    '#weight' => -9,
    '#tree' => TRUE,
  );

  $profiles = penceo_ipe_templates_get_profiles();
  if (empty($profiles)) {
    $form['profile_list']['no_result'] = array(
      '#markup' => theme('html_tag', array(
        'element' => array(
          '#tag' => 'div',
          '#attributes' => array('id' => 'available-profiles'),
          '#value' => t('There is no existing profile. Add a new one above!'),
        )
      )),
    );
  }
  else {
    foreach ($profiles as $machine_name => $name) {
      $form['profile_list'][$machine_name] = array(
        '#type' => 'checkbox',
        '#title' => t($name),
        // These are currently enabled at this point.
        '#default_value' => 1,
      );
    }
  }

  $form['profile']['profile_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Profile name'),
    '#description' => t('Type in the name of the profile.'),
    '#default_value' => '',
    '#required' => TRUE,
    '#size' => 30,
  );

  $form['profile']['profile_machine_name'] = array(
    '#type' => 'machine_name',
    '#title' => t('Profile machine name'),
    '#description' => t('Type in the unique name of a profile.'),
    '#maxlength' => 30,
    '#machine_name' => array(
      'exists' => '_penceo_ipe_machine_name_exists',
    ),
  );

  $form['profile']['profile_create'] = array(
    '#type' => 'submit',
    '#value' => t('Add new profile'),
    '#submit' => array('_penceo_ipe_templates_profile_add_profile'),
  );

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
    '#submit' => array('_penceo_ipe_templates_profile_save_settings'),
    '#limit_validation_errors' => array(),
  );

  return $form;
}

/**
 * Callback function for the machine name check.
 * @param string $profile_name
 * @return bool
 */
function _penceo_ipe_machine_name_exists($profile_name) {
  return penceo_ipe_templates_profile_exists($profile_name);
}

/**
 * Add profile submit callback.
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function _penceo_ipe_templates_profile_add_profile($form, &$form_state) {
  $profile = array(
    'name' => $form_state['values']['profile_name'],
    'machine_name' => $form_state['values']['profile_machine_name'],
  );
  $current_profiles = penceo_ipe_templates_get_profiles();
  $current_profiles[$profile['machine_name']] = $profile['name'];
  variable_set('penceo_ipe_templates_profiles', $current_profiles);
}

/**
 * Profile save settings callba ck.
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 */
function _penceo_ipe_templates_profile_save_settings($form, &$form_state) {
  if (!empty($form_state['input']['profile_list'])) {
    $profiles = penceo_ipe_templates_get_profiles();
    // Let's load our presets in case something needs to be deleted.
    $pane_presets = penceo_ipe_templates_get_pane_presets();
    // Indicates if we have to refresh already saved items.
    $refresh = FALSE;

    foreach (array_keys($form_state['input']['profile_list']) as $machine_name) {
      if (isset($profiles[$machine_name]) && !$form_state['input']['profile_list'][$machine_name]) {
        // Let's check if we need to remove some configuration as well.
        foreach ($pane_presets as $pane => $pane_settings) {
          if (isset($pane_presets['profiles'])
            && in_array($machine_name, $pane_presets['profiles'])
          ) {
            unset($pane_presets[$pane]['profiles'][$machine_name]);
            $refresh = TRUE;
          }
        }
        unset($profiles[$machine_name]);
      }
    }
    // Save profiles information.
    variable_set('penceo_ipe_templates_profiles', $profiles);
    // Resave pane presets in case of change.
    if ($refresh) {
      variable_set('penceo_ipe_templates_pane_presets', $pane_presets);
    }
  }
}

/**
 * Default form builder.
 *
 * @param $form
 * @param $form_state
 *
 * @return mixed
 *
 */
function penceo_ipe_templates_panes_form($form, &$form_state) {

  ctools_include('penceo_ipe_templates.helpers', 'penceo_ipe_templates');

  $form_state['storage']['panes'] = $panes = penceo_ipe_templates_get_panes();

  // Get currently set profiles.
  $profiles = penceo_ipe_templates_get_profiles();
  // Get already saved pane <=> profile combinations
  $pane_presets = penceo_ipe_templates_get_pane_presets();

  $form['pane_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Pane settings'),
    '#description' => t('Attach content panes that should be available when the specific profile is used.'),
    '#tree' => TRUE,
  );

  foreach ($panes as $main_category => $type) {
    $form['pane_settings'][$main_category] = array(
      '#type' => 'fieldset',
      '#title' => $type['name'],
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );

    foreach ($type['panes'] as $type_name => $ct) {
      $form['pane_settings'][$main_category][$type_name] = array(
        '#type' => 'fieldset',
        '#title' => $ct['name'],
        '#description' => $ct['description'],
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#tree' => TRUE,
      );

      $form['pane_settings'][$main_category][$type_name]['thumbnail_image'] = array(
        '#type' => 'managed_file',
        '#name' => 'thumbnail_image',
        '#title' => t('Thumbnail image'),
        '#description' => t('Attach a desired image to the selected pane. <i>This is visible in the IPE view.</i>'),
        '#upload_location' => 'public://penceo-ipe-templates/',
        '#default_value' => isset($pane_presets[$main_category][$type_name]['thumbnail_image']) ? $pane_presets[$main_category][$type_name]['thumbnail_image'] : '',
        '#upload_validators' => array(
          'file_validate_extensions' => array('jpg jpeg png'),
        ),
      );

      $form['pane_settings'][$main_category][$type_name]['profiles'] = array(
        '#type' => 'fieldset',
        '#title' => t('Available profiles'),
        '#description' => t('Here you can see the available profiles that have been previously set at the <i>Profile creation</i> tab.'),
        '#tree' => TRUE,
      );

      foreach ($profiles as $machine_name => $name) {
        $form['pane_settings'][$main_category][$type_name]['profiles'][$machine_name] = array(
          '#type' => 'checkbox',
          '#title' => t($name),
          '#default_value' => isset($pane_presets[$main_category][$type_name]['profiles'][$machine_name]),
        );

        if (isset($pane_presets[$main_category][$type_name]['profiles'][$machine_name])) {
          $form['pane_settings'][$main_category][$type_name]['#collapsed'] = FALSE;
        }
      }
    }
  }

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save changes'),
  );

  return $form;
}

/**
 * Custom submit callback for penceo_ipe_templates_panes_form().
 */
function penceo_ipe_templates_panes_form_submit($form, &$form_state) {
  $pane_types = $form_state['values']['pane_settings'];

  $settings = array();
  foreach ($pane_types as $type => $panes) {
    foreach ($panes as $pane_name => $pane) {
      if (isset($pane['profiles'])) {
        foreach ($pane['profiles'] as $profile_name => $value) {
          if ($value) {
            $settings[$type][$pane_name] = $form_state['storage']['panes'][$type]['panes'][$pane_name];
            $settings[$type][$pane_name]['profiles'][$profile_name] = TRUE;
          }
        }
      }
      // Check for old image first
      if ($pane['thumbnail_image'] !== $form['pane_settings'][$type][$pane_name]['thumbnail_image']['#default_value']) {
        $old_fid = $form['pane_settings'][$type][$pane_name]['thumbnail_image']['#default_value'];
        $old_file = file_load($old_fid);
        if ($old_file) {
          file_usage_delete($old_file, 'penceo_ipe_templates', 'penceo_ipe_templates_pane_presets', $old_fid);
          file_delete($old_file);
        }
      }

      // Let's check if we have an attached image for the pane.
      if ($pane['thumbnail_image'] > 0) {
        $file = file_load($pane['thumbnail_image']);
        $file->status = FILE_STATUS_PERMANENT;
        file_save($file);
        // Fake type param, we do not wish to display these images anywhere.
        file_usage_add($file, 'penceo_ipe_templates', 'penceo_ipe_templates_pane_presets', $file->fid);
        $settings[$type][$pane_name]['thumbnail_image'] = $file->fid;
      }
    }
  }

  variable_set('penceo_ipe_templates_pane_presets', $settings);

}

/**
 * Queries all available panes for setup.
 *
 * @return array
 */
function penceo_ipe_templates_get_panes() {

  $selected_pane_types = variable_get('penceo_ipe_templates_pane_types');

  $return_types = array();

  // FPP support enabled.
  if (isset($selected_pane_types['fieldable_panels_pane'])) {
    $type = 'fieldable_panels_pane';
    // Let's get our FPP bundles.
    $entity_info = entity_get_info($type);

    $bundles = $entity_info['bundles'];

    // Legacy FPPs have more configuration in code.
    $fpp = array();

    if (!empty($bundles)) {
      foreach ($bundles as $bundle_name => $bundle) {
        $fpp[$bundle_name] = array(
          'name' => check_plain($bundle['label']),
          'type' => $type,
          // Subtype is always the bundle for FPPs.
          'subtype' => $bundle_name,
          'description' => isset($bundle['description']) ? filter_xss_admin($bundle['description']) : '',
        );
      }
    }

    $return_types[$type] = array(
      'name' => penceo_ipe_templates_pane_type_defaults($type),
      'panes' => $fpp,
    );
  }

  // Custom panes lookup.
  if (isset($selected_pane_types['custom'])) {
    $project_content = array();

    foreach (penceo_ipe_templates_get_project_categories() as $category) {
      if ($panes = penceo_ipe_templates_get_panes_by_category($category)) {
        $project_content = array_merge($project_content, $panes);
      }
    }

    $return_types['custom'] = array(
      'name' => penceo_ipe_templates_pane_type_defaults('custom'),
      'panes' => $project_content,
    );
  }

  return $return_types;
}