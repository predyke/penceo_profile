<?php
/**
 * @file Helper function storage file.
 */

/**
 * Returns available profiles.
 *
 * @return array
 *    An array of profiles.
 */
function penceo_ipe_templates_get_profiles() {
  return variable_get('penceo_ipe_templates_profiles', array());
}

/**
 * Checks if the given profile exists.
 *
 * @param string $profile
 *  the machine name of the profile
 * @return bool
 *  True if exists false otherwise.
 */
function penceo_ipe_templates_profile_exists($profile = '') {
  $profiles = penceo_ipe_templates_get_profiles();
  return isset($profiles[$profile]);
}

/**
 * Returns default pane types.
 *
 * @param string $type
 *   The name of the type.
 *
 * @return array
 *    Default pane types.
 */
function penceo_ipe_templates_pane_type_defaults($type = '') {
  $types = array(
    'fieldable_panels_pane' => t('Fieldable panels pane'),
    'custom' => t('Custom'),
  );
  return isset($types[$type]) ? $types[$type] : $types;
}

/**
 * Returns pane <=> profile combinations with all available settings.
 *
 * @return array
 *    Pane presets.
 */
function penceo_ipe_templates_get_pane_presets() {
  return variable_get('penceo_ipe_templates_pane_presets', array());
}

/**
 * Returns the use defined project categories.
 *
 * @return array
 *    An array of project categories.
 */
function penceo_ipe_templates_get_project_categories() {
  return explode(',', trim(ltrim(variable_get('penceo_ipe_templates_project_category'))));
}

/**
 * Returns custom content types by category.
 *
 * @param string $category
 *    The name of the category the pane belongs to.
 * @param array
 *    An array of panes in a specific category.
 */
function penceo_ipe_templates_get_panes_by_category($category) {
  $panes = &drupal_static(__FUNCTION__, array());

  if (!isset($panes[$category])) {
    ctools_include('content');

    foreach (ctools_get_content_types() as $machine_name => $content_type) {
      if (isset($content_type['category']) && $content_type['category'] == $category) {
        $panes[$category][$machine_name] = array(
          'name' => check_plain($content_type['title']),
          'type' => $machine_name,
          'subtype' => $machine_name,
          'description' => isset($content_type['description']) ? filter_xss_admin($content_type['description']) : '',
        );
      }
    }
  }

  return isset($panes[$category]) ? $panes[$category] : array();
}

/**
 * Returns a list of unique panes which then can be used safely.
 *
 * @return array
 *    An array of unique panes.
 */
function penceo_ipe_templates_unique_pane_presets() {
  $clean_values = array();
  foreach (penceo_ipe_templates_get_pane_presets() as $type => $panes) {
    // Create a unique machine name based on the type and pane machine names.
    foreach ($panes as $pane_name => $data) {
      $clean_values[$type . '_' . $pane_name] = $data;
    }
  }
  return $clean_values;
}