<?php
/**
 * @file
 * penceo_cleanup.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function penceo_cleanup_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'clean_url';
  $strongarm->value = TRUE;
  $export['clean_url'] = $strongarm;

  return $export;
}
