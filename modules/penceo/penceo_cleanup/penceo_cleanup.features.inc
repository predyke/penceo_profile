<?php
/**
 * @file
 * penceo_cleanup.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function penceo_cleanup_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
