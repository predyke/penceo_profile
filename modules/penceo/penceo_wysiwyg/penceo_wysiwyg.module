<?php
/**
 * @file
 * Code for the Penceo Wysiwyg feature.
 */

include_once 'penceo_wysiwyg.features.inc';

/**
 * Implements hook_wysiwyg_entity_info_alter().
 */
function penceo_wysiwyg_entity_info_alter(&$entity_info) {
  // FORCE ENABLE custom settings for file entity otherwise
  // it won't be properly enabled on the file types page.
  $entity_info['file']['view modes']['wysiwyg']['custom settings'] = TRUE;
  // This view mode should be used as the rendered version.
  $entity_info['file']['view modes']['wysiwyg_display'] = array(
    'label' => t('WYSIWYG Display'),
    'custom settings' => TRUE,
  );
}

/**
 * Implements hook_wysiwyg_filter_elements_blacklist().
 */
function penceo_wysiwyg_wysiwyg_filter_elements_blacklist_alter(&$blacklist) {
  // Remove IFRAME from the blacklist.
  if (($iframe_key = array_search('iframe', $blacklist)) !== FALSE) {
    unset($blacklist[$iframe_key]);
  }
}

/**
 * Implements hook_field_formatter_info().
 */
function penceo_wysiwyg_field_formatter_info() {
  return array(
    'penceo_wysiwyg_file_link' => array(
      'label' => t('PENCEO: Wysiwyg file link'),
      'field types' => array('file'),
      'settings' => array(),
    ),
    'penceo_wysiwyg_file_display' => array(
      'label' => t('PENCEO: Wysiwyg file display'),
      'field types' => array('file'),
      'settings' => array(),
    ),
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function penceo_wysiwyg_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $settings = $display['settings'];

  switch ($display['type']) {
    case 'penceo_wysiwyg_file_link':

      foreach ($items as $delta => $item) {
        $link_text = $item['filename'];
        try {
          $wrapper = entity_metadata_wrapper($entity_type, $entity);
          // If there is an alternate text use it as the file link.
          if (isset($wrapper->field_file_title)) {
            $button_text = $wrapper->field_file_title->value();
            if ($button_text) {
              $link_text = $button_text;
            }
          }
        }
        catch (EntityMetadataWrapperException $e) {
          watchdog('entity_metadata_wrapper', 'entity_metadata_wrapper error in %error_loc', array('%error_loc' => __FUNCTION__ . ' @ ' . __FILE__ . ' : ' . __LINE__), WATCHDOG_CRITICAL);
          return '';
        }

        $element[$delta] = array(
          '#theme' => 'image',
          '#path' => 'penceo-wysiwyg-placeholder/' . base64_encode($link_text),
        );
      }
      break;

    case 'penceo_wysiwyg_file_display':
      foreach ($items as $delta => $item) {
        $link_text = $item['filename'];

        $attributes = array();

        try {
          $wrapper = entity_metadata_wrapper($entity_type, $entity);
          // If there is an alternate text use it as the file link.
          if (isset($wrapper->field_document_title)) {
            $button_text = $wrapper->field_document_title->value();
            if ($button_text) {
              $link_text = $button_text;
            }
          }

          if (isset($wrapper->field_document_target)) {
            $target = $wrapper->field_document_target->value();

            if ($target == '_blank' || $target == '_self') {
              $attributes['target'] = $target;
            }
            else {
              $attributes['download'] = '';
            }
          }
        }
        catch (EntityMetadataWrapperException $e) {
          watchdog('entity_metadata_wrapper', 'entity_metadata_wrapper error in %error_loc', array('%error_loc' => __FUNCTION__ . ' @ ' . __FILE__ . ' : ' . __LINE__), WATCHDOG_CRITICAL);
          return '';
        }

        $url = file_create_url($item['uri']);

        $classes = isset($entity->override['attributes']['class']) ? $entity->override['attributes']['class'] : array();

        $attributes['class'] = $classes;

        $element[$delta] = array(
          '#type' => 'html_tag',
          '#tag' => 'span',
          '#attributes' => array('class' => array('editor-file-link')),
          '#value' => l($link_text, $url, array('attributes' => $attributes)),
        );
      }
      break;
  }

  return $element;
}

/**
 * Implements hook_menu().
 */
function penceo_wysiwyg_menu() {
  return array(
    'penceo-wysiwyg-placeholder/%' => array(
      'access callback' => TRUE,
      'page callback' => 'penceo_wysiwyg_placeholder_image_callback',
      'page arguments' => array(1),
      'type' => MENU_CALLBACK,
    ),
  );
}

/**
 * Callback function to generate placeholder image.
 *
 * @param string $title
 *    Title of the file which will be used as a label on the image.
 */
function penceo_wysiwyg_placeholder_image_callback($title) {
  $img = imagecreatetruecolor(300, 38);

  $bg = imagecolorallocate($img, 245, 246, 248);
  imagefill($img, 0, 0, $bg);

  $black = imagecolorallocate($img, 0, 0, 0);
  imagestring($img, 2, 10, 10, t('Placeholder for file@placeholder', array(
      '@placeholder' => ': ',
    )) . base64_decode($title), $black);

  header('Content-type: image/jpeg');
  imagejpeg($img, NULL, 100);
  drupal_exit();
}
