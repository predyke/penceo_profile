<?php
/**
 * @file
 * penceo_wysiwyg.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function penceo_wysiwyg_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_document_target'.
  $field_bases['field_document_target'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_document_target',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        '_blank' => 'Open in new browser tab',
        '_self' => 'Open in current browser tab',
        'download' => ' Force download on file',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  // Exported field_base: 'field_document_title'.
  $field_bases['field_document_title'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_document_title',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  return $field_bases;
}
